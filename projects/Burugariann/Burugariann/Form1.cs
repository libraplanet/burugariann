﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Burugariann
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.SuspendLayout();

            EventHandler textBox1TextChanged = null;
            EventHandler textBox2TextChanged = null;

            textBox1TextChanged = new EventHandler(delegate(object sender, EventArgs e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string line in textBox1.Lines)
                {
                    foreach (char c in line)
                    {
                        string org = c.ToString();
                        string upper = org.ToUpper();
                        string lower = org.ToLower();

                        if (string.Equals(lower, upper))
                        {
                            sb.Append(c);
                        }
                        else
                        {
                            if (string.Equals(org, upper))
                            {
                                if ((sb.Length <= 0) || (sb[sb.Length - 1] != '_'))
                                {
                                    sb.Append('_');
                                }
                            }
                            sb.Append(upper);
                        }
                    }

                    sb.AppendLine();
                }
                textBox2.TextChanged -= textBox2TextChanged;
                textBox2.Text = sb.ToString();
                textBox2.TextChanged += textBox2TextChanged;

            });

            textBox2TextChanged = new EventHandler(delegate(object sender, EventArgs e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string line in textBox2.Lines)
                {
                    bool req = false;
                    foreach (char c in line)
                    {
                        string org = c.ToString();
                        if (c == '_')
                        {
                            if (req)
                            {
                                sb.Append(c);
                            }
                            else
                            {
                                req = true;
                            }
                        }
                        else
                        {
                            if (req)
                            {
                                sb.Append(org.ToUpper());
                            }
                            else
                            {
                                sb.Append(org.ToLower());
                            }
                            req = false;
                        }
                    }

                    sb.AppendLine();
                }
                textBox1.TextChanged -= textBox1TextChanged;
                textBox1.Text = sb.ToString();
                textBox1.TextChanged += textBox1TextChanged;
            });

            textBox1.TextChanged += textBox1TextChanged;
            textBox2.TextChanged += textBox2TextChanged;

            this.ResumeLayout();
        }
    }
}
